$nuget = (Get-ChildItem ".nuget\nuget.exe") | Select-Object -First 1
$version = "1.0.0"

if(Test-Path Env:PackageVersion){
	$version = $env:PackageVersion
	Write-Host "Package Version: $version"
}

function Build-Solution
{
	param($solutionFile, $frameworkVersion, [string[]]$arr)

	Write-Host "Running MSBUILD v$frameworkVersion"
	$regKey = "HKLM:\software\Microsoft\MSBuild\ToolsVersions\$frameworkVersion"
	$regProperty = "MSBuildToolsPath"

	$msbuildExe = join-path -path (Get-ItemProperty $regKey).$regProperty -childpath "msbuild.exe"

	Write-Host "Building Solution: $solutionFile"
	$args = $arr + @($solutionFile)
	Start -FilePath $msbuildExe -ArgumentList $args -NoNewWindow -Wait
}

function Publish-Web($project, $path)
{
	Write-Host "Publishing $project to $path"
	$arguments = @("/t:Rebuild","/p:DeployOnBuild=true","/p:Configuration=Release","/p:DeployTarget=Package","/p:AutoParameterizationWebConfigConnectionStrings=false","/p:_PackageTempDir=$path")
	Build-Solution $project "4.0" $arguments
}

function Nuget-Pack($nuspec) 
{
	$args = @("pack",$nuspec,"-Version $version")
	Start -FilePath $nuget -ArgumentList $args -NoNewWindow -Wait
}

$project = (Get-ChildItem **\**\*.Web.csproj -Recurse) | Select-Object -First 1
New-Item -ItemType directory -Path .\Publish -force
$path = Resolve-Path .\Publish

Write-Host "Publish Directory: $path"
Publish-Web -project $project -path $path

$nuspec = (Get-ChildItem src\**\*.nuspec -Recurse | Select-Object -First 1)
#Write-Host "Nuspec File: $nuspec"

Nuget-Pack $nuspec