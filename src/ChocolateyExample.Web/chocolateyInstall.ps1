﻿Set-Location (Split-Path $MyInvocation.MyCommand.Path)

# Install .NET (If Needed)
cinstm DotNet4.5

# Install IIS (If Missing)
cinst IIS-WebServerRole -source windowsfeatures

Import-Module WebAdministration

function Make-Dir($path){
	new-item $path -itemType directory -force
}

function Create-Site ($name, $path) {
	$exists = Test-Site($name)
	if(!$exists) {
		Make-Dir($path)
		
		New-WebAppPool -Name $name
		Set-ItemProperty IIS:\AppPools\$name managedRuntimeVersion v4.0
		New-WebSite -Name $name -PhysicalPath $path -ApplicationPool $name
	}
}

function Create-App-Pool ($name, $version) {
	New-WebAppPool -Name $name
	Set-ItemProperty IIS:\AppPools\$name managedRuntimeVersion $version
}

function Test-Site ($name) {
	(Get-Website | Where-Object { $_.name -eq $name }) -ne $null
}

function Test-Web-Application ($name) {
	(Get-WebApplication -Name $name) -ne $null
}

try {
	$basePath = "C:\Webs\"
	$app = "ChocolateyExample.Web"
	$appPath = Join-Path $basePath $app

	Write-Host "Checking for previous versions of $app"
	$isUpgrade = Test-Web-Application ($app)

	if(!$isUpgrade) {
		Write-Host "No previous versions found, installing fresh"
		Write-Host "Installing $app to: $appPath"
		Make-Dir $appPath
		Copy-Item ..\Content\* $appPath -force -recurse
		Create-Site -name $app -path $appPath
	}
	else {
		Write-Host "Previous version of $app found"
		Write-Host "Stopping $app"
		Stop-WebSite $app
		Write-Host "Deploying files to $appPath"
		Copy-Item Copy-Item ..\Content\* $appPath -force -recurse

		Write-Host "Starting $app"
		Start-WebSite $app
	}
} catch {
	throw
}